export const environment = {
  isProduction: true,
  endpoint: 'http://localhost:8080/v1',
  auth0: {
    domain: 'https://dev-kokskw5a0uvqw0cp.eu.auth0.com',
    clientId: 'BCl5fPrfUZ2xIFumzYzer8IrkkM7tbnq',
    redirect_uri: window.location.origin,
    audience: 'https://dev-kokskw5a0uvqw0cp.eu.auth0.com/api/v2/'
  },
  api: 'http://localhost:8080'
};
