import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HeroPartialComponent } from './hero-partial.component';

describe('HeroPartialComponent', () => {
  let component: HeroPartialComponent;
  let fixture: ComponentFixture<HeroPartialComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [HeroPartialComponent]
    });
    fixture = TestBed.createComponent(HeroPartialComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
