import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {HomePageComponent} from "./pages/home-page/home-page.component";
import {MoncompteComponent} from "./pages/moncompte/moncompte.component";
import {CartPageComponent} from "./pages/cart-page/cart-page.component"
import {ProductPageComponent} from "./pages/product-page/product-page.component";
import {AProductPageComponent} from "./pages/a-product-page/a-product-page.component"
import {AuthGuard} from "@auth0/auth0-angular";
import {LivraisonsComponent} from "./pages/livraisons/livraisons.component";
import {GestionPaimentsComponent} from "./pages/gestion-paiments/gestion-paiments.component";
import {CommandValidatedComponent} from "./pages/command-validated/command-validated.component";

const routes: Routes = [
  { path: '', redirectTo: 'home', pathMatch: 'full'},
  { path: 'home',  component: HomePageComponent, title: 'Accueil' },
  { path: 'mon-compte', component: MoncompteComponent, title: 'Mon compte',
    canActivate: [AuthGuard]
  },

  { path: 'panier', component: CartPageComponent, title: 'Panier' },
  { path: 'listeProduits', component: ProductPageComponent, title: 'Produits' },
  { path: 'product/:productId', component: AProductPageComponent, title: "Page d'un produit" },
  { path: 'commande/:userId', component: LivraisonsComponent, title: "Gestion commande"},
  { path: 'commande/:userId/:key', component: GestionPaimentsComponent, title: "Moyens de paiement" },
  { path: 'commande/:userId/:key/commandValid/:orderId', component: CommandValidatedComponent, title: "Commande validée"}
  ]

;

@NgModule({
  imports: [RouterModule.forRoot(routes)],

  exports: [RouterModule]
})
export class AppRoutingModule { }

