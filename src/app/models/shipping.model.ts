export class ShippingModel {

  private _key: number;
  private _modeLivraison: string;
  private _livreur: string;
  private _livreDansXJours: number;
  private _prix: number;


  constructor(key: number, modeLivraison: string, livreur: string, livreDansXJours: number, prix: number) {
    this._key = key;
    this._modeLivraison = modeLivraison;
    this._livreur = livreur;
    this._livreDansXJours = livreDansXJours;
    this._prix = prix;
  }

  get key(): number {
    return this._key;
  }

  set key(value: number) {
    this._key = value;
  }

  get modeLivraison(): string {
    return this._modeLivraison;
  }

  set modeLivraison(value: string) {
    this._modeLivraison = value;
  }

  get livreur(): string {
    return this._livreur;
  }

  set livreur(value: string) {
    this._livreur = value;
  }

  get livreDansXJours(): number {
    return this._livreDansXJours;
  }

  set livreDansXJours(value: number) {
    this._livreDansXJours = value;
  }

  get prix(): number {
    return this._prix;
  }

  set prix(value: number) {
    this._prix = value;
  }
}
