export class Product {
  private _productId: number;
  private _productName: string;
  private _productDescription : string;
  private _productPrice: number;
  private _productImage: string;
  private _productCategory: string;
  private _stock: number;


  constructor(productId: number, productName: string, productDescription: string, productPrice: number, productImage: string, productCategory: string, productStock: number) {
    this._productId = productId;
    this._productName = productName;
    this._productDescription = productDescription;
    this._productPrice = productPrice;
    this._productImage = productImage;
    this._productCategory = productCategory;
    this._stock = productStock;
  }

  get productId(): number {
    return this._productId;
  }

  set productId(value: number) {
    this._productId = value;
  }

  get productName(): string {
    return this._productName;
  }

  set productName(value: string) {
    this._productName = value;
  }

  get productDescription(): string {
    return this._productDescription;
  }

  set productDescription(value: string) {
    this._productDescription = value;
  }

  get productPrice(): number {
    return this._productPrice;
  }

  set productPrice(value: number) {
    this._productPrice = value;
  }

  get productImage(): string {
    return this._productImage;
  }

  set productImage(value: string) {
    this._productImage = value;
  }

  get productCategory(): string {
    return this._productCategory;
  }

  set productCategory(value: string) {
    this._productCategory = value;
  }

  get stock(): number {
    return this._stock;
  }

  set stock(value: number) {
    this._stock = value;
  }
}
