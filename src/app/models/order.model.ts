import {UserDatabase} from "./userDatabase.model";

export class Order {

  private _orderId: number;
  private _user: UserDatabase;
  private _orderPrice: number;
  private _orderQuantity: number;
  private _orderDate: string;
  private _orderShippingStatus: string;
  private _orderPaymentStatus: string;


  constructor(orderId: number, user: UserDatabase, orderPrice: number, orderQuantity: number, orderDate: string, orderShippingStatus: string, orderPaymentStatus: string) {
    this._orderId = orderId;
    this._user = user;
    this._orderPrice = orderPrice;
    this._orderQuantity = orderQuantity;
    this._orderDate = orderDate;
    this._orderShippingStatus = orderShippingStatus;
    this._orderPaymentStatus = orderPaymentStatus;
  }


  get orderId(): number {
    return this._orderId;
  }

  set orderId(value: number) {
    this._orderId = value;
  }

  get user(): UserDatabase {
    return this._user;
  }

  set user(value: UserDatabase) {
    this._user = value;
  }

  get orderPrice(): number {
    return this._orderPrice;
  }

  set orderPrice(value: number) {
    this._orderPrice = value;
  }

  get orderQuantity(): number {
    return this._orderQuantity;
  }

  set orderQuantity(value: number) {
    this._orderQuantity = value;
  }

  get orderDate(): string {
    return this._orderDate;
  }

  set orderDate(value: string) {
    this._orderDate = value;
  }

  get orderShippingStatus(): string {
    return this._orderShippingStatus;
  }

  set orderShippingStatus(value: string) {
    this._orderShippingStatus = value;
  }

  get orderPaymentStatus(): string {
    return this._orderPaymentStatus;
  }

  set orderPaymentStatus(value: string) {
    this._orderPaymentStatus = value;
  }
}
