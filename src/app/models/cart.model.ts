import {UserDatabase} from "./userDatabase.model";

export class Cart {
  constructor(cartId: number, user: UserDatabase, totalPrice: number, totalQuantity: number) {
    this._cartId = cartId;
    this._user = user;
    this._totalPrice = totalPrice;
    this._totalQuantity = totalQuantity;
  }

  private _cartId: number;
  private _user: UserDatabase;
  private _totalPrice: number;
  private _totalQuantity: number;

  get cartId(): number {
    return this._cartId;
  }

  set cartId(value: number) {
    this._cartId = value;
  }

  get user(): UserDatabase {
    return this._user;
  }

  set user(value: UserDatabase) {
    this._user = value;
  }

  get totalPrice(): number {
    return this._totalPrice;
  }

  set totalPrice(value: number) {
    this._totalPrice = value;
  }

  get totalQuantity(): number {
    return this._totalQuantity;
  }

  set totalQuantity(value: number) {
    this._totalQuantity = value;
  }
}
