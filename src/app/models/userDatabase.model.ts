export class UserDatabase {
  constructor(userId: number, userEmail: string | undefined) {
    this._userId = userId;
    this._userEmail = userEmail;

  }
  private _userId: number;
  private _userEmail: string | undefined;

  get userId(): number {
    return this._userId;
  }

  set userId(value: number) {
    this._userId = value;
  }

  get userEmail(): string | undefined {
    return this._userEmail;
  }

  set userEmail(value: string | undefined) {
    this._userEmail = value;
  }




}
