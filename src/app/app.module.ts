import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomePageComponent } from './pages/home-page/home-page.component';
import {HTTP_INTERCEPTORS, HttpClientModule} from "@angular/common/http";

import { ProductPageComponent } from './pages/product-page/product-page.component';
import {NgOptimizedImage} from "@angular/common";
import { ProgressBarComponent } from './pages/progress-bar/progress-bar.component';
import { ProductItemsCartComponent } from './pages/product-items-cart/product-items-cart.component';
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import { HeroPartialComponent } from './partials/hero-partial/hero-partial.component';
import {AuthModule} from "@auth0/auth0-angular";
import { environment as env } from '../environments/environment';
import {AuthInterceptorInterceptor} from './middlewares/auth-interceptor.interceptor';
import { LivraisonsComponent } from './pages/livraisons/livraisons.component';
import { GestionPaimentsComponent } from './pages/gestion-paiments/gestion-paiments.component';
import { PicturesDirective } from './directives/pictures.directive';
import {HeaderSharedComponent} from "./shared/header-shared/header-shared.component";
import {FooterSharedComponent} from "./shared/footer-shared/footer-shared.component";
import {CarrouselHomePageComponent} from "./pages/carrousel-home-page/carrousel-home-page.component";
import {CartPageComponent} from "./pages/cart-page/cart-page.component";
import {MoncompteComponent} from "./pages/moncompte/moncompte.component";
import {AsideMenuProductsPageComponent} from "./pages/aside-menu-products-page/aside-menu-products-page.component";
import {InformationsComponent} from "./pages/moncompte-components/informations/informations.component";
import {CommandesComponent} from "./pages/moncompte-components/commandes/commandes.component";
import {NavbarComponent} from "./pages/moncompte-components/navbar/navbar.component";
import {AProductPageComponent} from "./pages/a-product-page/a-product-page.component";
import {CommandValidatedComponent} from "./pages/command-validated/command-validated.component";
import {ComposantPdfTestComponent} from "./test/composant-pdf-test/composant-pdf-test.component";

@NgModule({
  declarations: [
    AppComponent,
    HeaderSharedComponent,
    FooterSharedComponent,
    HomePageComponent,
    CarrouselHomePageComponent,
    CartPageComponent,
    ProductPageComponent,
    AsideMenuProductsPageComponent,
    ProgressBarComponent,
    ProductItemsCartComponent,
    HeroPartialComponent,
    MoncompteComponent,
    InformationsComponent,
    CommandesComponent,
    NavbarComponent,
    AProductPageComponent,
    LivraisonsComponent,
    GestionPaimentsComponent,
    PicturesDirective,
    CommandValidatedComponent,
    ComposantPdfTestComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    AuthModule.forRoot({
      domain: env.auth0.domain,
      clientId: env.auth0.clientId,
      authorizationParams: {
        scope: 'openid profile email read:posts write:posts',
        redirect_uri: env.auth0.redirect_uri,
        //audience: env.auth0.audience,
        ui_locales: 'fr-FR'
      },
    }),

    NgOptimizedImage,
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [{ provide: HTTP_INTERCEPTORS,
    useClass: AuthInterceptorInterceptor,
    multi: true
  },
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
