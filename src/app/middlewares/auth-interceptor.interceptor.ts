import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor
} from '@angular/common/http';
import {Observable, switchMap} from 'rxjs';
import {AuthService} from "@auth0/auth0-angular";

@Injectable()
export class AuthInterceptorInterceptor implements HttpInterceptor {

  constructor(private authService: AuthService) {}

  intercept(request: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<unknown>> {
    //this.authService.getAccessTokenSilently().subscribe(token => console.log(`silent token: ${token}`))
    return this.authService.idTokenClaims$.pipe(
      switchMap(token => {
        if (request.method === "GET" && (request.url.startsWith("http://localhost:8080/v1/products") && !request.url.startsWith("http://localhost:8080/v1/products/stockProduct"))){
          const newRequest = request
          //console.log("get products request: " + JSON.stringify(newRequest))
          return next.handle(newRequest);
        }
        else {
          const newRequest = request.clone(
            {
              setHeaders: {
                Authorization: `Bearer ${token?.__raw}`
              }
            });
          //console.log(token?.__raw)
          //console.log("not get products request: " + JSON.stringify(newRequest))
          return next.handle(newRequest);
        }
      })
    );
  }

/*  intercept(request: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<unknown>> {
    return this.authService.idTokenClaims$.pipe(
      switchMap(token => {
        const newRequest = request.clone(
          {
          setHeaders: {
            Authorization: `Bearer ${token?.__raw}`
          }
        });
        console.log(newRequest)
        //console.log(token?.__raw)
        return next.handle(newRequest);
      })
    );
  }*/

}
