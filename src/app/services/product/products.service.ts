import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {environment as Env} from "../../../environments/environment";
import {ProductsRequest} from "../../interfaces/products/products-request";
import {Product} from "../../models/product.model";

@Injectable({
  providedIn: 'root'
})
export class ProductsService {

  constructor(private _http: HttpClient) { }

  public getAllProducts(): Observable<Product[]> {
    return this._http.get<Product[]>(`${Env.endpoint}/products`);
  }

  public getAProduct(productId: number): Observable<Product> {
    return this._http.get<Product>(`${Env.endpoint}/products/${productId}`)
  }

  public getAllProductsByCat(categorie:string): Observable<Product[]> {
    return this._http.get<Product[]>(`${Env.endpoint}/products/cat/${categorie}`);
  }

  public getAProductStock(productId:number): Observable<any> {
    return this._http.get<any>(`${Env.endpoint}/products/stockProduct/${productId}`)
  }

  //backoffice
  public updateAProduct(productRequest: ProductsRequest, productId:number): Observable<Product> {
    return this._http.put<Product>(`${Env.endpoint}/products/${productId}`, productRequest)
  }

  public deleteAProduct(productId:number): Observable<any> {
    return this._http.delete(`${Env.endpoint}/products/${productId}`)
  }

}
