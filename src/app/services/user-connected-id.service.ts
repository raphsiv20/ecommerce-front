import {Injectable} from '@angular/core';
import {AuthService, User} from "@auth0/auth0-angular";
import {UserServiceService} from "./user/user.service.service";
import {CartService} from "./cart/cart.service.service";
import {BehaviorSubject, Observable, Subject} from "rxjs";
import {UserDatabase} from "../models/userDatabase.model";
import {UserDatabaseRequest} from "../interfaces/usersDatabase/user-database-request";
import {CompleteCartsRequest} from "../interfaces/carts/complete-carts-request";
import {Cart} from "../models/cart.model";

@Injectable({
  providedIn: 'root'
})
export class UserConnectedIdService{

  private _allUsers: UserDatabase[] = [];
  private _allCarts: Cart[] = [];
  private _lastUserId: number = 0;
  private _lastCartId: number = 0;
  private _userConnectedId: BehaviorSubject<number>;
  private _userConnectedCartId: BehaviorSubject<number>;


  /*private _userConnectedId: number;
  private _userConnectedCartId: number;*/
  constructor(private authService: AuthService, private userService: UserServiceService, private cartService: CartService) {
    //Je recup tous les users et carts
    this.userService.getAllUser().subscribe(users => {
      this._allUsers = users;
      this._lastUserId = this._allUsers[this._allUsers.length - 1]["userId"]
    });
    this.cartService.getAllCarts().subscribe(carts => {
      this._allCarts = carts
      this._lastCartId = this._allCarts[this._allCarts.length - 1]["cartId"]
    });

    this._userConnectedId = new BehaviorSubject<number>(0)
    this._userConnectedCartId = new BehaviorSubject<number>(0)
    this.authService.user$.subscribe(userConnected => {
      //console.log(userConnected?.email === undefined)
      //Je vérif que le user est connecté
      if (userConnected?.email !== undefined) {

      //Je verifie que le user est dans ma bd
        this.userService.userExists(userConnected?.email).subscribe(userExists => {
          if (JSON.parse(JSON.stringify(userExists))["userExists"]) {

            this.userService.getAUserByEmail(userConnected?.email).subscribe(userDatabase => {
              this._userConnectedId.next(userDatabase.userId)

              //Je verifie que le user a un panier
              this.cartService.cartExists(userDatabase.userId).subscribe(cartExists => {
                if (JSON.parse(JSON.stringify(cartExists))["cartExists"]) {

                  this.cartService.getACartByUserId(userDatabase.userId).subscribe(cart => {
                    this._userConnectedCartId.next(cart.cartId)
                    //console.log("service userId: " + userDatabase.userId)
                  })
                  //Si le user n'a pas de panier je lui en crée 1 et l id du panier est l'id du dernier panier +1
                } else {
                  let cartInfos: CompleteCartsRequest = {
                    "user": {"userId": userDatabase.userId},
                    "totalPrice": 0,
                    "totalQuantity": 0
                  }
                  this.cartService.createCart(cartInfos).subscribe()
                  this._userConnectedCartId.next(this._lastCartId+1)

                }
              })
            })
            //Si le user nest pas dans ma bd, je fais lenregistrement avec son mail et son id est l'id du dernier user +1 et je lui crée son panier qui aura comme id l id du dernier panier
          } else {
            let userInfos: UserDatabaseRequest = {"userEmail": userConnected?.email}
            this.userService.createUser(userInfos).subscribe(user => {
              console.log(user)
              this._userConnectedId.next(user["userId"])
              let cartInfos: CompleteCartsRequest = {
                "user": {"userId": user["userId"]},
                "totalPrice": 0,
                "totalQuantity": 0
              }

              this.cartService.createCart(cartInfos).subscribe();
              this._userConnectedCartId.next(this._lastCartId+1)
            })

          }
        })//Si il n y a pas de user connecté
      }
    })
  }


  get userConnectedId(): BehaviorSubject<number> {
    return this._userConnectedId;
  }

  set userConnectedId(value: BehaviorSubject<number>) {
    this._userConnectedId = value;
  }


  get userConnectedCartId(): BehaviorSubject<number> {
    return this._userConnectedCartId;
  }

  set userConnectedCartId(value: BehaviorSubject<number>) {
    this._userConnectedCartId = value;
  }


  get allUsers(): UserDatabase[] {
    return this._allUsers;
  }

  set allUsers(value: UserDatabase[]) {
    this._allUsers = value;
  }

  get allCarts(): Cart[] {
    return this._allCarts;
  }

  set allCarts(value: Cart[]) {
    this._allCarts = value;
  }

  get lastUserId(): number {
    return this._lastUserId;
  }

  set lastUserId(value: number) {
    this._lastUserId = value;
  }

  get lastCartId(): number {
    return this._lastCartId;
  }

  set lastCartId(value: number) {
    this._lastCartId = value;
  }

}
