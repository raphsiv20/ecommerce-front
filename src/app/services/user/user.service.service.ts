import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {Observable} from "rxjs";
import {environment as Env} from "../../../environments/environment";
import {UserDatabase} from "../../models/userDatabase.model";
import {UserDatabaseRequest} from "../../interfaces/usersDatabase/user-database-request";

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type':  'application/json'
  })
};

@Injectable({
  providedIn: 'root'
})
export class UserServiceService {

  constructor(private _http: HttpClient) { }

  public getAllUser(): Observable<UserDatabase[]> {
    return this._http.get<UserDatabase[]>(`${Env.endpoint}/users`);
  }

  public getAUser(userId:number): Observable<UserDatabase> {
    return this._http.get<UserDatabase>(`${Env.endpoint}/users/${userId}`);
  }

  public getAUserByEmail(userEmail: unknown): Observable<UserDatabase> {
    return this._http.get<UserDatabase>(`${Env.endpoint}/users/getUser/${userEmail}`);
  }

  public createUser(userDataBaseRequest: UserDatabaseRequest): Observable<any> {
    return this._http.post<any>(`${Env.endpoint}/users`, userDataBaseRequest, httpOptions)
  }


  public userExists(userEmail: string | undefined): Observable<boolean> {
    return this._http.get<boolean>(`${Env.endpoint}/users/userExists/${userEmail}`)
  }



}
