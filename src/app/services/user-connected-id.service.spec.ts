import { TestBed } from '@angular/core/testing';

import { UserConnectedIdService } from './user-connected-id.service';

describe('UserConnectedIdService', () => {
  let service: UserConnectedIdService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(UserConnectedIdService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
