import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {ShippingModel} from "../../models/shipping.model";
import {Product} from "../../models/product.model";
import {environment as Env} from "../../../environments/environment";

@Injectable({
  providedIn: 'root'
})
export class ShippingsService {

  constructor(private _http: HttpClient) { }

  public getAllShippings(): Observable<ShippingModel[]> {
    return this._http.get<ShippingModel[]>(`${Env.endpoint}/shippings`);
  }

  public getAShippingsByKey(key:number): Observable<ShippingModel> {
    return this._http.get<ShippingModel>(`${Env.endpoint}/shippings/${key}`);
  }
}
