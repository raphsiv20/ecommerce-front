import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {Observable} from "rxjs";
import {environment as Env} from "../../../environments/environment";
import {OrderShippingRequest} from "../../interfaces/orders/orderShippingRequest";
import {CompleteCartsRequest} from "../../interfaces/carts/complete-carts-request";
import {Cart} from "../../models/cart.model";
import {CompleteOrderRequest} from "../../interfaces/orders/CompleteOrderRequest";
import {Order} from "../../models/order.model";


const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type':  'application/json'
  })
};

export interface OrderItem {
  order: any;
  product: any;
  quantity: number;
}

@Injectable({
  providedIn: 'root'
})
export class OrderService {

  constructor(private _http: HttpClient) { }

  public getAllOrders(): Observable<Order[]> {
    return this._http.get<Order[]>(`${Env.endpoint}/orders/allOrders`);
  }

  public getAnOrder(orderId: number): Observable<Order> {
    return this._http.get<Order>(`${Env.endpoint}/orders/anOrder/${orderId}`);
  }

  public getAllOrdersByUser(userId: number): Observable<Order[]> {
    return this._http.get<Order[]>(`${Env.endpoint}/orders/user/${userId}`);
  }

  public getAnOrderItems(orderId: number): Observable<OrderItem[]> {
    return this._http.get<OrderItem[]>(`${Env.endpoint}/orderItems/${orderId}`);
  }

  public createOrderPdf(orderId: number, filename: string): Observable<any> {
    return this._http.get<any>(`${Env.endpoint}/orders/generatePdf/${orderId}/${filename}`);
  }

  public renderOrderPdf(filename: string): Observable<any> {
    return this._http.get<any>(`${Env.endpoint}/orders/renderPdf/${filename}`);
  }

  public createOrder(orderRequest:CompleteOrderRequest): Observable<Order> {
    return this._http.post<Order>(`${Env.endpoint}/orders`, orderRequest, httpOptions);
  }

  public addOrderItems(orderItem: OrderItem): Observable<any> {
    return this._http.post<any>(`${Env.endpoint}/orderItems`, orderItem, httpOptions);
  }

  //back Office

  public updateOrderShippingStatus(orderShippingRequest: OrderShippingRequest): Observable<any> {
    return this._http.put<any>(`${Env.endpoint}/orderItems`, orderShippingRequest, httpOptions);
  }

  public updateOrderPaymentStatus(orderShippingRequest: OrderShippingRequest): Observable<any> {
    return this._http.put<any>(`${Env.endpoint}/orderItems`, orderShippingRequest, httpOptions);
  }
}
