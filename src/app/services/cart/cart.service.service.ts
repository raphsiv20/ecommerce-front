import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders, HttpParams} from "@angular/common/http";
import {Observable, Subject} from "rxjs";
import {Cart} from "../../models/cart.model";
import {environment as Env} from "../../../environments/environment";
import {UserCartsRequest} from "../../interfaces/carts/user-carts-request";
import {CompleteCartsRequest} from "../../interfaces/carts/complete-carts-request";

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type':  'application/json'
  })
};
export interface CartItems {
  cart: any;
  product: any;
  quantity: number;
}


@Injectable({
  providedIn: 'root'
})
export class CartService {

  constructor(private _http: HttpClient) { }

  public getAllCarts(): Observable<Cart[]> {
    return this._http.get<Cart[]>(`${Env.endpoint}/carts`);
  }

  public getACartById(cartId:number): Observable<Cart> {
    return this._http.get<Cart>(`${Env.endpoint}/carts/${cartId}`);
  }
  public getACartByUserId(userId: number): Observable<Cart> {
    return this._http.get<Cart>(`${Env.endpoint}/carts/user/${userId}`);
  }

  public createCart(cartRequest:CompleteCartsRequest): Observable<Cart> {
    return this._http.post<Cart>(`${Env.endpoint}/carts`, cartRequest, httpOptions);
  }
  public getACartItems(id:number): Observable<CartItems[]> {
    return this._http.get<CartItems[]>(`${Env.endpoint}/cartItems/${id}`);
  }

  public addToCart(cartItem: CartItems): Observable<any> {
    return this._http.post<any>(`${Env.endpoint}/cartItems`, cartItem, httpOptions);
  }

  public updateCartItems(cartItems:CartItems): Observable<any> {
    return this._http.put(`${Env.endpoint}/cartItems`, cartItems, httpOptions);
  }

  public resetCartQuantityPrice(cartId: number): Observable<any> {
    return this._http.put(`${Env.endpoint}/carts/priceAndQuantity/${cartId}`, httpOptions);
  }

  public updateCartUser(userCartRequest:UserCartsRequest, cartId:number): Observable<any> {
    return this._http.put(`${Env.endpoint}/carts/${cartId}`, userCartRequest, httpOptions);
  }

  public deleteCartItems(cartId:number, productId:number): Observable<any> {
    return this._http.delete(`${Env.endpoint}/cartItems/${cartId}/${productId}`, httpOptions)
  }

  public cartExists(userId: number | undefined): Observable<boolean> {
    return this._http.get<boolean>(`${Env.endpoint}/carts/cartExists/${userId}`)
  }

}
