import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class TrimService {
  public trimString(value: string | undefined): string | undefined {
    if (value !== undefined) {
      return value.replace(/\s/g, '');
    }
    return ;
  }
}
