import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CarrouselHomePageComponent } from './carrousel-home-page.component';

describe('CarrouselHomePageComponent', () => {
  let component: CarrouselHomePageComponent;
  let fixture: ComponentFixture<CarrouselHomePageComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [CarrouselHomePageComponent]
    });
    fixture = TestBed.createComponent(CarrouselHomePageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
