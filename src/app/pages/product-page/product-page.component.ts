import {Component, OnInit} from '@angular/core';
import {Product} from "../../models/product.model";
import {ProductsService} from "../../services/product/products.service";
import {CartItems, CartService} from "../../services/cart/cart.service.service";
import {AuthService} from "@auth0/auth0-angular";
import {UserServiceService} from "../../services/user/user.service.service";
import {UserConnectedIdService} from "../../services/user-connected-id.service";
import {TrimService} from "../../services/trim.service";


@Component({
  selector: 'app-product-page',
  templateUrl: './product-page.component.html',
  styleUrls: ['./product-page.component.scss']
})
export class ProductPageComponent implements OnInit {

  private _products: Product[] = [];
  private _productsLength: number = 0;
  private _userConnectedCartId: number = 0;

  constructor(private _productService: ProductsService, private _cartService: CartService, private _authService: AuthService, private _userConnectedService: UserConnectedIdService, private _trimService: TrimService) {
  }

  ngOnInit(): void {
    this._userConnectedService.userConnectedCartId.subscribe(id => {
      if (id !== 0) {
        this._userConnectedCartId = id;
        console.log(`product page cart id ${id}`)
      }
    })

    /*console.log("1er userId: " + localStorage.getItem("userConnectedId"))
    if (localStorage.getItem("userConnectedCartId") !== null) {
      const cartId = localStorage.getItem("userConnectedCartId")
      if (cartId !== null) {
        this._userConnectedCartId = parseInt(cartId)
      }
    }*/

    this._productService.getAllProducts().subscribe(products => {
      console.log(this.trimProductName(products[0].productName))
      for (let i = 0; i < products.length; i++) {
        if (products[i].stock > 0) {
          this._products.push(products[i])
        }
      }
      this._productsLength = this._products.length
    })
  }

  public trimProductName(productName: string | undefined): string |undefined {
    return this._trimService.trimString(productName);
  }


  //afficher les json du produit associe au add to cart
  addProductToCart(productId: number) {
    let productInCart: boolean = false;
    //console.log("hors boucle: " + productInCart)
    console.log(this._userConnectedCartId)
    //test pour voir si le produit est dans le panier
    this._cartService.getACartItems(this._userConnectedCartId).subscribe(items => {
      for (let item of items) {
        //si il est dans le panier, je mets la variable sur true
        if (item["product"]["productId"] === productId) {
          alert("Le produit est déjà dans le panier.")
          productInCart = true;
          console.log("boucle: " + productInCart)
          }
      }
      //Si le produit n'est pas dans le panier alors je rajoute 1 fois le produit
      if (!productInCart) {
        const cartItem: CartItems = {
          cart: {
            cartId: this._userConnectedCartId
          },
          product: {
            productId: productId
          },
          quantity: 1
        }

        console.log(cartItem)
        this._cartService.addToCart(cartItem).subscribe();
        alert('Product added to cart successfully!')
      }
    })
  }


  get userConnectedCartId(): number {
    return this._userConnectedCartId;
  }

  set userConnectedCartId(value: number) {
    this._userConnectedCartId = value;
  }

  get productsLength(): number {
    return this._productsLength;
  }

  set productsLength(value: number) {
    this._productsLength = value;
  }

  get products(): Product[] {
    return this._products;
  }

  set products(value: Product[]) {
    this._products = value;
  }

  protected readonly JSON = JSON;

  refreshProducts(products: Product[]) {
    this._products = products;
    this._productsLength = this._products.length;
  }

}
