import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {ComposantPdfTestComponent} from "../../test/composant-pdf-test/composant-pdf-test.component";
import {OrderService} from "../../services/order/order.service";


@Component({
  selector: 'app-home-page',
  templateUrl: './home-page.component.html',
  styleUrls: ['./home-page.component.scss']
})
export class HomePageComponent implements OnInit {

  progress = 0;
  //@ViewChild("testRender", {static: true}) private aTag!: ElementRef;

  private _donneesFa: string = "Hello world";
  private _pdfFile!: any;


  constructor(private _orderService: OrderService) {
  }

  get donneesFa(): string {
    return this._donneesFa;
  }

  set donneesFa(value: string) {
    this._donneesFa = value;
  }

  setProgress ($event: Event) {
    this.progress = +($event.target as HTMLInputElement).value;
  }

  public openPdfFather(): void {
    //console.log(JSON.stringify(this.childComponentParent))
    //this.childComponentParent.generatePDFChild();
  }

  ngOnInit(): void {
    //this.aTag.nativeElement.href = "helloWorld";
  }

}
