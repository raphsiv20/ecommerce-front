import {Component, Input} from '@angular/core';
import {CartService} from "../../services/cart/cart.service.service";
import {UserCartsRequest} from "../../interfaces/carts/user-carts-request";
import {AuthService, User} from "@auth0/auth0-angular";

@Component({
  selector: 'app-moncompte',
  templateUrl: './moncompte.component.html',
  styleUrls: ['./moncompte.component.scss']
})
export class MoncompteComponent {

  private _nomZoneACharger: string ="mesInformations";
  private _userConnected:any;

  constructor(private _authService: AuthService) {
    this._authService.user$.subscribe(userConnected => {
      this._userConnected = JSON.parse(JSON.stringify(userConnected));
    })
  }


  get userConnected(): User {
    return this._userConnected;
  }

  set userConnected(value: User) {
    this._userConnected = value;
  }

  get nomZoneACharger(): string {
    return this._nomZoneACharger;
  }

  set nomZoneACharger(value: string) {
    this._nomZoneACharger = value;
  }

//Composant à charger lors d un clique sur un bouton du navbar, je recup le nom de la zone à charger
  area(zoneACharger: string) {
    this._nomZoneACharger = zoneACharger
  }
}
