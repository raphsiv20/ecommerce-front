import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AsideMenuProductsPageComponent } from './aside-menu-products-page.component';

describe('AsideMenuProductsPageComponent', () => {
  let component: AsideMenuProductsPageComponent;
  let fixture: ComponentFixture<AsideMenuProductsPageComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [AsideMenuProductsPageComponent]
    });
    fixture = TestBed.createComponent(AsideMenuProductsPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
