import {Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges} from '@angular/core';
import {Product} from "../../models/product.model";
import {ProductsService} from "../../services/product/products.service";
import {ProductsResponse} from "../../interfaces/products/products-response";

@Component({
  selector: 'app-aside-menu-products-page',
  templateUrl: './aside-menu-products-page.component.html',
  styleUrls: ['./aside-menu-products-page.component.scss']
})
export class AsideMenuProductsPageComponent implements OnInit {

  @Output() newItemEvent = new EventEmitter<Product[]>();
  private _products: Product[] = [];
  private _categories: string[] = [];

  constructor(private productService: ProductsService) {
  }

  get categories(): string[] {
    return this._categories;
  }

  set categories(value: string[]) {
    this._categories = value;
  }

  ngOnInit(): void {
    this.productService.getAllProducts().subscribe(products => {
      for (let i = 0; i < products.length; i++) {
        if (products[i].stock > 0) {
          this._products.push(products[i])
        }
      }
      for (const product of this._products) {
        this._categories.push(product.productCategory)
      }
      this._categories = this.uniqByReduce(this._categories);
    })

  }

  get products(): Product[] {
    return this._products;
  }


  set products(value: Product[]) {
    this._products = value;
  }

  uniqByReduce<T>(array: T[]): T[] {
    return array.reduce((acc: T[], cur: T) => {
      if (!acc.includes(cur)) {
        acc.push(cur);
      }
      return acc;
    }, [])
  }



  protected readonly JSON = JSON;

  showProducts() {
    this._products = []
    this.productService.getAllProducts().subscribe(products => {
      for (let i = 0; i < products.length; i++) {
        if (products[i].stock > 0) {
          this._products.push(products[i])
        }
      }
      this.newItemEvent.emit(this._products);
    })
  }


  showProductsAsc() {
    this._products.sort((firstItem, secondItem) => firstItem.productPrice - secondItem.productPrice);
    this.newItemEvent.emit(this._products);
  }

  showProductsDesc() {
    this._products.sort((firstItem, secondItem) => secondItem.productPrice - firstItem.productPrice);
    this.newItemEvent.emit(this._products);
  }

  showProductsCat(categorie: string) {
    this.productService.getAllProductsByCat(categorie).subscribe(products => {
      for (let i = 0; i < products.length; i++) {
        if (products[i].stock == 0) {
          products.splice(i, 1);
        }
      }
      this._products = products
      this.newItemEvent.emit(products);
    })
  }

  showProductsRange(prixMin: number, prixMax: number) {
    if (prixMin === null) {
      alert("Veuillez renseigner le prix minimum.")
      return ;
    }
    if (prixMax === null) {
      alert("Veuillez renseigner le prix maximum.")
      return ;
    }

    if (prixMin < 0 || prixMax < 0) {
      alert("Les prix ne peuvent pas être inférieures à 0.")
      return ;
    }
    if (prixMax <= prixMin) {
      alert("Le prix maximum doit être supérieur au prix minimum.")
      return ;
    }

    let products:Product[] = [];
    for (let i = 0; i < this._products.length; i++) {
      if (this._products[i].productPrice >= prixMin && this._products[i].productPrice <= prixMax) {
        products.push(this._products[i]);
      }
    }
    this.newItemEvent.emit(products)
  }

  protected readonly Number = Number;
}
