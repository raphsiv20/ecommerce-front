import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {UserServiceService} from "../../services/user/user.service.service";
import {CartService} from "../../services/cart/cart.service.service";
import {UserConnectedIdService} from "../../services/user-connected-id.service";
import {ActivatedRoute, Router} from "@angular/router";
import {ShippingsService} from "../../services/shippings/shippings.service";
import {Order} from "../../models/order.model";
import {CompleteOrderRequest} from "../../interfaces/orders/CompleteOrderRequest";
import * as moment from "moment";
import {OrderItem, OrderService} from "../../services/order/order.service";

@Component({
  selector: 'app-gestion-paiments',
  templateUrl: './gestion-paiments.component.html',
  styleUrls: ['./gestion-paiments.component.scss']
})
export class GestionPaimentsComponent implements OnInit {
  @ViewChild("submitButton", { static: true }) button! : ElementRef;
  private _totalCosts: number;
  private _cbStyle: string;
  private _paypalStyle: string;
  private _invalidInput: string;
  private _todayDate: string;
  private _cartId: number;

  constructor(private route: ActivatedRoute, private router: Router, private userConnectedId: UserConnectedIdService, private _cartService:CartService, private _shippingService:ShippingsService, private _orderService: OrderService) {
    this._totalCosts = 0;
    this._cbStyle = "";
    this._paypalStyle = "";
    this._invalidInput = "";
    this._todayDate = "";
    this._cartId = 0;

  }

  ngOnInit(): void {
    const keyRoute = this.route.snapshot.paramMap.get("key")
    let keyShipping: number;
    if (keyRoute !== null) {
       keyShipping = parseInt(keyRoute)
    }
    const userIdRoute = this.route.snapshot.paramMap.get("userId")
    let userId: number;
    if (userIdRoute !== null) {
      userId = parseInt(userIdRoute)
      this._cartService.getACartByUserId(userId).subscribe(cart => {
        this._cartId = cart.cartId
        this._shippingService.getAShippingsByKey(keyShipping).subscribe(shippingInfo => {
          this._totalCosts += shippingInfo["prix"] + cart["totalPrice"]
        })
      })
    }
    moment.locale("fr")
    this._todayDate = (moment().format('Do MMMM YYYY'));
  }

  get todayDate(): string {
    return this._todayDate;
  }

  set todayDate(value: string) {
    this._todayDate = value;
  }

  get cartId(): number {
    return this._cartId;
  }

  set cartId(value: number) {
    this._cartId = value;
  }

  get cbStyle(): string {
    return this._cbStyle;
  }

  set cbStyle(value: string) {
    this._cbStyle = value;
  }

  get paypalStyle(): string {
    return this._paypalStyle;
  }

  set paypalStyle(value: string) {
    this._paypalStyle = value;
  }

  get totalCosts(): number {
    return this._totalCosts;
  }

  set totalCosts(value: number) {
    this._totalCosts = value;
  }


  get invalidInput(): string {
    return this._invalidInput;
  }

  set invalidInput(value: string) {
    this._invalidInput = value;
  }

  public showCbPaiement(): void {
    if (this._cbStyle !== "display: block;") {
      this._cbStyle = "display: block;"
    } else {
      this._cbStyle = "display: none;"
    }
  }

  public showPaypalPaiement(): void {
    if (this._paypalStyle !== "display: block;") {
      this._paypalStyle = "display: block;"
    } else {
      this._paypalStyle = "display: none;"
    }
  }

  lengthCard(cardValue: string) {
    if (cardValue.length != 4) {
      alert("Veuillez saisir le nombre de chiffres adéquat.")
      this._invalidInput = "is-invalid"
      this.button.nativeElement.style = "pointer-events: none;\n" +
        "              cursor: default;\n" +
        "              text-decoration: none;\n" +
        "              color: black;\n" +
        "              opacity: .2"
    } else {
      for (let i = 0; i < cardValue.length; i++) {
        if (!this.isNumber(cardValue[i])) {
          this._invalidInput = "is-invalid";
          this.button.nativeElement.style = "pointer-events: none;\n" +
            "              cursor: default;\n" +
            "              text-decoration: none;\n" +
            "              color: black;\n" +
            "              opacity: .2"
          alert("Veuillez saisir des chiffres.")
          return ;
        }
      }
      this._invalidInput = ""
      this.button.nativeElement.style =
        "              padding: 0.5em;\n" +
        "              text-decoration: none;\n" +
        "              border: 3px solid"
    }

  }

  checkDateValue(value: string) {
    if (value.length != 2) {
      alert("Veuillez saisir le nombre de chiffres adéquat.")
      this._invalidInput = "is-invalid"
      this.button.nativeElement.style = "pointer-events: none;\n" +
        "              cursor: default;\n" +
        "              text-decoration: none;\n" +
        "              color: black;\n" +
        "              opacity: .2"
    } else {
      for (let i = 0; i < value.length; i++) {
        if (!this.isNumber(value[i])) {
          this._invalidInput = "is-invalid";
          alert("Veuillez saisir des chiffres.")
          return ;
        }
      }
      this._invalidInput = ""
      this.button.nativeElement.style =
        "              padding: 0.5em;\n" +
        "              text-decoration: none;\n" +
        "              border: 3px solid"
    }
  }

  checkCvcValue(value: string) {
    if (value.length != 3) {
      alert("Veuillez saisir le nombre de chiffres adéquat.")
      this._invalidInput = "is-invalid"
      this.button.nativeElement.style = "pointer-events: none;\n" +
        "              cursor: default;\n" +
        "              text-decoration: none;\n" +
        "              color: black;\n" +
        "              opacity: .2"
    } else {
      for (let i = 0; i < value.length; i++) {
        if (!this.isNumber(value[i])) {
          this._invalidInput = "is-invalid";
          alert("Veuillez saisir des chiffres.")
          return ;
        }
      }
      this._invalidInput = ""
      this.button.nativeElement.style =
        "              padding: 0.5em;\n" +
        "              text-decoration: none;\n" +
        "              border: 3px solid"
    }
  }

  public isNumber(value?: string | number): boolean
  {
    return ((value != null) &&
      (value !== '') &&
      !isNaN(Number(value.toString())));
  }

  public resetCartInfo(cartId: number): void {
    this._cartService.resetCartQuantityPrice(cartId).subscribe()
  }


  protected readonly String = String;

  public validPayment() {
    //faire la route vers la validation
    console.log("Enfiiiiin")
    const keyRoute = this.route.snapshot.paramMap.get("key")
    let keyShipping: number;
    if (keyRoute !== null) {
      keyShipping = parseInt(keyRoute)
    }
    const userIdRoute = this.route.snapshot.paramMap.get("userId")
    let userId: number;
    if (userIdRoute !== null) {
      userId = parseInt(userIdRoute)
      console.log(`UserId ${userId}`)
      this._cartService.getACartByUserId(userId).subscribe(cart => {
        const orderRequest: CompleteOrderRequest = {
          orderDate: this._todayDate,
          orderPaymentStatus: "En cours",
          orderPrice: this._totalCosts,
          orderQuantity: cart["totalQuantity"],
          orderShippingStatus: "En cours",
          user: {"userId": userId}
        }
        console.log(`orderRequest: ${JSON.stringify(orderRequest)}`)
        this._orderService.createOrder(orderRequest).subscribe(order => {
          this._cartService.getACartItems(cart.cartId).subscribe(cartItems => {
            if (cartItems.length > 0) {
              for (let i = 0; i < cartItems.length; i++) {
                console.log(`cartItem: ${JSON.stringify(cartItems[i])}`)
                const orderRequest: OrderItem = {
                  order: {"orderId": order.orderId},
                  product: {"productId": cartItems[i]["product"]["productId"]},
                  quantity: cartItems[i]["quantity"]
                }
                this._cartService.deleteCartItems(cartItems[i]["cart"]["cartId"], cartItems[i]["product"]["productId"]).subscribe()
                this._orderService.addOrderItems(orderRequest).subscribe()
              }
            }
          })

          this.router.navigate(["/commande", userIdRoute, keyRoute, "commandValid", order.orderId]);
        })
      })
    }

  }


}
