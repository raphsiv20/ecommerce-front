import {AfterViewInit, Component, OnDestroy, OnInit} from '@angular/core';
import {CartService} from "../../services/cart/cart.service.service";
import {UserServiceService} from "../../services/user/user.service.service";
import {AuthService} from "@auth0/auth0-angular";
import {UserConnectedIdService} from "../../services/user-connected-id.service";
import {Subscription} from "rxjs";
import * as moment from "moment";
@Component({
  selector: 'app-cart-page',
  templateUrl: './cart-page.component.html',
  styleUrls: ['./cart-page.component.scss']
})

export class CartPageComponent implements OnInit, AfterViewInit {
  private _cartEmpty: boolean = true;
  private _totalPrice: number;
  private _totalQuantity: number;
  private _cartItems: any[];
  private _cartItemsLength: number;
  private _userConnectedId:number;
  private _userConnectedCartId: number;
  private _userConnected: boolean;
  private _todayDate: any;

  constructor(private _cartService: CartService, private _authService: AuthService, private _userConnectedService: UserConnectedIdService) {
    this._totalQuantity = 0;
    this._totalPrice = 0;
    this._cartItems = [];
    this._cartItemsLength = 0;
    this._userConnected = false;
    this._todayDate = "";
    this._userConnectedId = 0;
    this._userConnectedCartId = 0;
    /*let liste = [1,2,3]
    console.log(`list method filter: ${JSON.stringify(liste.filter((item) => item > 1))}`)
    console.log(`list method map: ${JSON.stringify(liste.map((item) => item * 3))}`)*/
  }


  get todayDate(): any {
    return this._todayDate;
  }

  set todayDate(value: any) {
    this._todayDate = value;
  }

  ngOnInit(): void {
    /*let LocalStorage = JSON.parse(JSON.stringify(localStorage))
    console.log(LocalStorage)
    this._userConnectedId = parseInt(LocalStorage["userConnectedId"])
    this._userConnectedCartId = parseInt(LocalStorage["userConnectedCartId"])*/
    moment.locale("fr")
    this._todayDate = (moment().format('Do MMMM YYYY'));
    console.log(typeof this._todayDate)


    //console.log(this._userConnectedId)
    //console.log(this._userConnectedCartId)
}

  ngAfterViewInit(): void {
    this._userConnectedService.userConnectedId.subscribe(userId => {
      console.log(`User id after view init ${userId}`)
        if (userId !== 0){
          this._userConnectedId = userId
          this._userConnected = true
        } else {
          this._userConnected = false
        }
    })
    this._userConnectedService.userConnectedCartId.subscribe(cartId => {
      console.log(`cartId: ${cartId}`)
      if (cartId !==0) {
        this._cartService.getACartItems(cartId).subscribe(items => {
          if (items.length > 0) {
            //console.log(items[0]["cart"]["cartId"])
            this._cartEmpty = false;
            this._cartItems = JSON.parse(JSON.stringify(items));
            this._totalPrice = this._cartItems[0]["cart"]["totalPrice"];
            this._totalQuantity = this._cartItems[0]["cart"]["totalQuantity"];
          }
          this._cartItemsLength = items.length;
          //console.log(this._cartItemsLength)
          this._userConnectedCartId = cartId
        })
      }
    })
  }



  get userConnectedId(): number {
    return this._userConnectedId;
  }

  set userConnectedId(value: number) {
    this._userConnectedId = value;
  }

  get userConnectedCartId(): number {
    return this._userConnectedCartId;
  }

  set userConnectedCartId(value: number) {
    this._userConnectedCartId = value;
  }

  get cartEmpty(): boolean {
    return this._cartEmpty;
  }

  set cartEmpty(value: boolean) {
    this._cartEmpty = value;
  }



  get cartItemsLength(): number {
    return this._cartItemsLength;
  }

  set cartItemsLength(value: number) {
    this._cartItemsLength = value;
  }

  get cartItems(): any[] {
    return this._cartItems;
  }

  set cartItems(value: any[]) {
    this._cartItems = value;
  }

  get totalPrice(): number {
    return this._totalPrice;
  }

  set totalPrice(value: number) {
    this._totalPrice = value;
  }

  get totalQuantity(): number {
    return this._totalQuantity;
  }

  set totalQuantity(value: number) {
    this._totalQuantity = value;
  }


  get userConnected(): boolean {
    return this._userConnected;
  }

  set userConnected(value: boolean) {
    this._userConnected = value;
  }

  deleteProductFromCart(productToDelete: { [key: string]: number } = {}) {
    //console.log(productToDelete)
    let newCartItems = []

    //supprimer l'élément du cart corrspondant au produit supprimé
    for (let i = 0; i < this._cartItems.length; i++) {
      this._cartItems[i]["cart"]["totalPrice"] = productToDelete["newTotalPrice"];
      this._cartItems[i]["cart"]["totalQuantity"] = productToDelete["newTotalQuantity"];
      if (this._cartItems[i]["product"]["productId"] != productToDelete["productId"]) {
        newCartItems.push(this._cartItems[i])
        //this._cartItems.splice(i, 1)
      }
    }
    //mettre à jour les inputs
    console.log(JSON.stringify(newCartItems))
    this._cartItems = newCartItems
    this._cartItemsLength = this._cartItems.length;
    if (this._cartItemsLength == 0) {
      this._cartEmpty = true;
    }
    //console.log("taille liste: " + this._cartItemsLength)
    //console.log(JSON.parse(JSON.stringify(this._cartItems)))

    //mettre à jour le prix total et qté totale
    this._totalPrice = productToDelete["newTotalPrice"];
    this._totalQuantity = productToDelete["newTotalQuantity"];

  }


  refreshPriceAndQuantity(totalPriceAndQuantity: { [key: string]: number }) {
    //console.log(totalPriceAndQuantity)
    let newCartItems = []
    //Mettre à jour la qté de chaque produit à jour par rapport au change
    for (let i = 0; i < this._cartItems.length; i++) {
      this._cartItems[i]["cart"]["totalPrice"] = totalPriceAndQuantity["newTotalPrice"];
      this._cartItems[i]["cart"]["totalQuantity"] = totalPriceAndQuantity["newTotalQuantity"];
      if (this._cartItems[i]["product"]["productId"] == totalPriceAndQuantity["productId"]) {
        this._cartItems[i]["quantity"] = totalPriceAndQuantity["newQuantity"]
      }
      newCartItems.push(this._cartItems[i])
    }

    this._cartItems = newCartItems

    //mettre à jour le prix total et qte totale
    this._totalPrice = totalPriceAndQuantity["newTotalPrice"];
    this._totalQuantity = totalPriceAndQuantity["newTotalQuantity"];
  }

}
