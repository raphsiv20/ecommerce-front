import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {Observable} from "rxjs";
import {Product} from "../../models/product.model";
import {ActivatedRoute, NavigationEnd, Router} from "@angular/router";
import {ProductsService} from "../../services/product/products.service";
import {ProductsResponse} from "../../interfaces/products/products-response";
import {CartItems, CartService} from "../../services/cart/cart.service.service";
import {UserConnectedIdService} from "../../services/user-connected-id.service";
import {TrimService} from "../../services/trim.service";

@Component({
  selector: 'app-a-product-page',
  templateUrl: './a-product-page.component.html',
  styleUrls: ['./a-product-page.component.scss']
})
export class AProductPageComponent implements OnInit {
  @ViewChild('topPage', {static: true}) private topPage!: ElementRef;
  private _product: Product | null | undefined;
  private _cartId!: number;

  constructor(private route: ActivatedRoute, private router: Router, private productService: ProductsService, private _userConnectedService: UserConnectedIdService, private _cartService: CartService, private _trimService: TrimService) {
  }

  ngOnInit() {
    this.router.events.subscribe(event => {
      if (event instanceof NavigationEnd) {
        window.scrollTo({ top: 0, behavior: 'smooth' });
      }
    });
    this.route.params.subscribe(params => {
      this.productService.getAProduct(params["productId"]).subscribe(product => {
        this._product = product
      })
    })
    this._userConnectedService.userConnectedCartId.subscribe(cartId => {
      if (cartId !== 0) {
        this._cartId = cartId
      }
    })
  }

  public trimProductName(productName: string | undefined): string | undefined {
    return this._trimService.trimString(productName);
  }

  get product(): Product | null | undefined {
    return this._product;
  }

  set product(value: Product | null | undefined) {
    this._product = value;
  }

  protected readonly JSON = JSON;


  addProductToCart(productId: number | undefined) {
    let productInCart: boolean = false;
    //console.log("hors boucle: " + productInCart)
    console.log(this._cartId)
    //test pour voir si le produit est dans le panier
    this._cartService.getACartItems(this._cartId ).subscribe(items => {
      for (let item of items) {
        //si il est dans le panier, je mets la variable sur true
        if (item["product"]["productId"] === productId) {
          alert("Le produit est déjà dans le panier.")
          productInCart = true;
          console.log("boucle: " + productInCart)
        }
      }
      //Si le produit n'est pas dans le panier alors je rajoute 1 fois le produit
      if (!productInCart) {
        const cartItem: CartItems = {
          cart: {
            cartId: this._cartId
          },
          product: {
            productId: productId
          },
          quantity: 1
        }

        console.log(cartItem)
        this._cartService.addToCart(cartItem).subscribe();
        alert('Product added to cart successfully!')
      }
    })
  }
}
