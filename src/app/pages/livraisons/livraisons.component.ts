import {Component, ElementRef, OnInit, QueryList, ViewChildren} from '@angular/core';
import {FormBuilder, FormGroup, UntypedFormBuilder, Validators} from "@angular/forms";
import {ActivatedRoute, Router} from "@angular/router";
import {ShippingsService} from "../../services/shippings/shippings.service";
import {ShippingModel} from "../../models/shipping.model";

@Component({
  selector: 'app-livraisons',
  templateUrl: './livraisons.component.html',
  styleUrls: ['./livraisons.component.scss']
})
export class LivraisonsComponent implements OnInit{
  private _infosLivraisons: ShippingModel[];
  private _shippingInfos: FormGroup;
  private _buttonChecked!: boolean;
  @ViewChildren('radio') private _radioButtons!: QueryList<ElementRef>;

  constructor(private _fb: FormBuilder, private  router: Router, private route: ActivatedRoute, private _shippingsService:ShippingsService) {
    this._infosLivraisons = [];
    this._shippingInfos = this._fb.group({
      shipping: this._fb.control('', [Validators.required])
    })
  }

  ngOnInit(): void {
    this._shippingsService.getAllShippings().subscribe(shippings => {
      this._infosLivraisons = JSON.parse(JSON.stringify(shippings))
    })
  }


  get radioButtons(): QueryList<ElementRef> {
    return this._radioButtons;
  }

  set radioButtons(value: QueryList<ElementRef>) {
    this._radioButtons = value;
  }

  get shippingInfos(): FormGroup {
    return this._shippingInfos;
  }

  set shippingInfos(value: FormGroup) {
    this._shippingInfos = value;
  }

  get infosLivraisons(): ShippingModel[] {
    return this._infosLivraisons;
  }

  set infosLivraisons(value: ShippingModel[]) {
    this._infosLivraisons = value;
  }

  protected readonly JSON = JSON;

  onSubmit() {
    console.log(this._radioButtons.toArray())
    for (let i = 0; i < this._radioButtons.toArray().length; i++) {
      if (this._radioButtons.toArray()[i].nativeElement.checked) {
        let infos = JSON.parse(this._shippingInfos.value["shipping"])
        console.log(infos["key"])
        const userId = this.route.snapshot.paramMap.get("userId")
        //Mettre le userId en recuperant le user id de lactive route pour faire le chemein dynamique avec le userId et la key
        this.router.navigate(["/commande", userId, infos["key"]]);
        return ;
      } else {
        this._buttonChecked = false;
      }
    }
    if (!this._buttonChecked) {
      alert("Veuillez cocher une option de livraison.")
    }
  }

}
