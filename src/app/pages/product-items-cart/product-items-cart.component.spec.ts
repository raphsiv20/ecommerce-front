import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ProductItemsCartComponent } from './product-items-cart.component';

describe('ProductItemsCartComponent', () => {
  let component: ProductItemsCartComponent;
  let fixture: ComponentFixture<ProductItemsCartComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [ProductItemsCartComponent]
    });
    fixture = TestBed.createComponent(ProductItemsCartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
