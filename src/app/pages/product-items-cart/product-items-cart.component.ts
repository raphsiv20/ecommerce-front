import {Component, EventEmitter, HostListener, Input, OnChanges, OnInit, Output, SimpleChanges} from '@angular/core';
import {CartItems, CartService} from "../../services/cart/cart.service.service";
import {ProductsService} from "../../services/product/products.service";
import {TrimService} from "../../services/trim.service";
@Component({
  selector: 'app-product-items-cart',
  templateUrl: './product-items-cart.component.html',
  styleUrls: ['./product-items-cart.component.scss']
})
export class ProductItemsCartComponent implements OnInit, OnChanges {
  @Input() _isCartEmpty: boolean = true;
  @Input() _items: any[] = [];
  @Input() _itemsLength: number = 0;
  @Output() newCartItems = new EventEmitter<{}>();
  @Output() newCartItemsDeleted = new EventEmitter<{}>();
  //private _productStock: number;
  private _responsiveCss!: boolean;

  @HostListener('window:resize', ['$event'])
  onResize(event: any) {
    this.detectScreenSize();
  }

  detectScreenSize() {
    const screenWidth = window.innerWidth;
    this._responsiveCss = screenWidth < 992;
    console.log(this._responsiveCss);
  }
  get responsiveCss(): boolean {
    return this._responsiveCss;
  }

  set responsiveCss(value: boolean) {
    this._responsiveCss = value;
  }


  constructor(private cartService: CartService, private productsService: ProductsService, private _trimService: TrimService) {
    //this._productStock = 0;
  }
 /* get productStock(): number {
    return this._productStock;
  }

  set productStock(value: number) {
    this._productStock = value;
  }*/

  public trimProductName(productName: string | undefined): string |undefined {
    return this._trimService.trimString(productName);
  }

  ngOnInit(): void {
    this.detectScreenSize();
    /*this.productsService.getAProductStock(5).subscribe(productStock => {
      this._productStock = productStock["stock"]
    })*/
  }
  ngOnChanges(changes: SimpleChanges): void {
    console.log(changes)
    if (changes["_items"]) {
      this._items = changes["_items"]["currentValue"]
    }
    if (changes["_isCartEmpty"]) {
      this._itemsLength = changes["_itemsLength"]["currentValue"]
    }
    if (changes["_isCartEmpty"]) {
      this._isCartEmpty = changes["_isCartEmpty"]["currentValue"]
    }

  }

  deleteProductItem(productId: number, productPrice: number, quantity: number): void {
    // maj du prix total, qte totale et qte de chaque article dans la page cart et creation du dico qui va etre envoye au parent
    let currentTotalPrice:number = this._items[0]["cart"]["totalPrice"]
    let currentTotalQuantity:number = this._items[0]["cart"]["totalQuantity"]
    let newTotalPrice = currentTotalPrice - (productPrice * quantity);
    let newTotalQuantity = currentTotalQuantity - quantity;

    //creation du corps de la requete pour la requete delete à l'api
    this.cartService.deleteCartItems(this._items[0]["cart"]["cartId"], productId).subscribe()

    //envoie du contenu à la fonction ts du parent pour mettre à jour la liste des items du panier et le prix et qte.
    this.newCartItemsDeleted.emit(
      {
        "newTotalPrice": newTotalPrice,
        "newTotalQuantity": newTotalQuantity,
        "productId" : productId
      });
  }


  updateTotalPriceAndQuantity(productId: number, productPrice:number, oldQuantity: number, newQuantity: number): void {
    // maj du prix total, qte totale et qte de chaque article dans la page cart et creation du dico qui va etre envoye au parent
    let currentTotalPrice:number = this._items[0]["cart"]["totalPrice"]
    let currentTotalQuantity:number = this._items[0]["cart"]["totalQuantity"]
    let newTotalPrice:number;
    let newTotalQuantity:number;

    if (newQuantity < 1) {
      alert("Veuillez saisir une quantité supérieur à 0.")
      return ;
    }

    if (newQuantity > oldQuantity) {
      newTotalPrice = currentTotalPrice + ((newQuantity - oldQuantity) * productPrice)
      newTotalQuantity = currentTotalQuantity + (newQuantity - oldQuantity)

    } else {
      newTotalPrice = currentTotalPrice - ((oldQuantity - newQuantity ) * productPrice)
      newTotalQuantity = currentTotalQuantity - (oldQuantity - newQuantity)
    }


    this.newCartItems.emit(
      {
        "newTotalPrice": newTotalPrice,
        "newTotalQuantity": newTotalQuantity,
        "productId" : productId,
        "newQuantity" : newQuantity
      });

    //creation du corps de la requete pour la requete put à l'api
    const cartItem:CartItems = {
      cart: {
        cartId: this._items[0]["cart"]["cartId"]
      },
      product: {
        productId: productId
      },
      quantity: newQuantity
    }
    this.cartService.updateCartItems(cartItem).subscribe();


  }

  protected readonly Number = Number;
  protected readonly JSON = JSON;

}
