import {AfterViewInit, Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {OrderService} from "../../services/order/order.service";
import * as moment from "moment";
import {ShippingsService} from "../../services/shippings/shippings.service";
import {environment as Env} from "../../../environments/environment";
import {User} from "@auth0/auth0-angular";
import {UserConnectedIdService} from "../../services/user-connected-id.service";
import {CartService} from "../../services/cart/cart.service.service";

@Component({
  selector: 'app-command-validated',
  templateUrl: './command-validated.component.html',
  styleUrls: ['./command-validated.component.scss']
})
export class CommandValidatedComponent implements OnInit, AfterViewInit {
  @ViewChild('renderPdf', {static: true}) private _renderPdf!: ElementRef;
  private _totalCosts!: number;
  private _dayOfShipment!: string;
  private _userId!: number;

  constructor(private router: Router, private route: ActivatedRoute, private _orderService: OrderService, private _shippingService: ShippingsService, private _userConnectedService: UserConnectedIdService, private _cartService: CartService) {
  }

  ngOnInit(): void {
    moment.locale("fr")
    const KEYROUTE = this.route.snapshot.paramMap.get("key")
    let keyShipping: number;
    if (KEYROUTE !== null) {
      keyShipping = parseInt(KEYROUTE)
    }
    const USERIDROUTE = this.route.snapshot.paramMap.get("userId")
    let userId: number;
    if (USERIDROUTE !== null) {
      userId = parseInt(USERIDROUTE)
      this._userId = userId
      this._cartService.getACartByUserId(userId).subscribe(cart => {
        this._cartService.resetCartQuantityPrice(cart.cartId).subscribe()
      })
    }

    const ORDERIDROUTE = this.route.snapshot.paramMap.get("orderId")
    let orderId: number;
    if (ORDERIDROUTE !== null) {
      orderId = parseInt(ORDERIDROUTE)
      this._orderService.getAnOrder(orderId).subscribe(order => {
        this._shippingService.getAShippingsByKey(keyShipping).subscribe(shipping => {
          this._totalCosts = order.orderPrice
          this._dayOfShipment = moment().add(shipping.livreDansXJours, 'days').format('Do MMMM YYYY');
          const FILENAME: string = `commande${orderId}user${userId}`;
          this._orderService.createOrderPdf(orderId, FILENAME).subscribe();
          this._renderPdf.nativeElement.href = `${Env.endpoint}/orders/renderPdf/${FILENAME}`;
        })
      })
    }
  }

  ngAfterViewInit(): void {
    this._cartService.getACartByUserId(this._userId).subscribe(cart => {
      this._cartService.resetCartQuantityPrice(cart.cartId).subscribe()
    })
  }


  get userId(): number {
    return this._userId;
  }

  set userId(value: number) {
    this._userId = value;
  }

  get renderPdf(): ElementRef {
    return this._renderPdf;
  }

  set renderPdf(value: ElementRef) {
    this._renderPdf = value;
  }

  get totalCosts(): number {
    return this._totalCosts;
  }

  set totalCosts(value: number) {
    this._totalCosts = value;
  }

  get dayOfShipment(): string {
    return this._dayOfShipment;
  }

  set dayOfShipment(value: string) {
    this._dayOfShipment = value;
  }


}
