import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CommandValidatedComponent } from './command-validated.component';

describe('CommandValidatedComponent', () => {
  let component: CommandValidatedComponent;
  let fixture: ComponentFixture<CommandValidatedComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [CommandValidatedComponent]
    });
    fixture = TestBed.createComponent(CommandValidatedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
