import {Component, EventEmitter, Input, Output} from '@angular/core';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent {

  @Output() areaToLoad: EventEmitter<string> = new EventEmitter();
  @Input() userConnected:any = "";

  chargerZoneCommande() {
    this.areaToLoad.emit("mesCommandes")
  }

  chargerZoneInformations() {
    this.areaToLoad.emit("mesInformations")
  }
}
