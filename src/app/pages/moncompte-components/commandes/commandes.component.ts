import {AfterViewInit, Component, ElementRef, OnInit, QueryList, ViewChild, ViewChildren} from '@angular/core';
import {Order} from "../../../models/order.model";
import {OrderItem, OrderService} from "../../../services/order/order.service";
import {UserConnectedIdService} from "../../../services/user-connected-id.service";
import {TrimService} from "../../../services/trim.service";

@Component({
  selector: 'app-commandes',
  templateUrl: './commandes.component.html',
  styleUrls: ['./commandes.component.scss']
})
export class CommandesComponent implements OnInit, AfterViewInit {
  @ViewChildren('contenuCommande') private _contenuCommandes!: QueryList<ElementRef>;
  @ViewChildren('boutonContenuCommande') private _boutonContenuCommandes!: QueryList<ElementRef>;
  private _listeCommandes!: Order[];
  private _anOrderItems!: OrderItem[];


  constructor(private _orderService: OrderService, private _userConnectedService: UserConnectedIdService, private _trimService: TrimService) {
  }

  ngOnInit(): void {
    this._userConnectedService.userConnectedId.subscribe(userId => {
      if (userId !== 0) {
        this._orderService.getAllOrdersByUser(userId).subscribe(orders => {
          this._listeCommandes = orders;
        })
      }
    })

  }

  public trimString (string: string | undefined): string | undefined {
    return this._trimService.trimString(string);
  }

  //faire le style display apres le ng view init, faire en sorte que le boutton afficher, ne plus afficher apres avoir cliquer
  ngAfterViewInit(): void {
    //console.log(this._contenuCommandes)
    console.log(this._boutonContenuCommandes)
  }

  showOrderItems(orderId: number, index: number) {
    console.log(this._contenuCommandes.toArray()[index].nativeElement.style.display)
    if (this._contenuCommandes.toArray()[index].nativeElement.style.display === "none") {
      this._contenuCommandes.toArray()[index].nativeElement.style.display = "block"
      this._boutonContenuCommandes.toArray()[index].nativeElement.textContent = "Ne plus afficher le contenu de la commande"
    } else {
      this._contenuCommandes.toArray()[index].nativeElement.style.display = "none"
      this._boutonContenuCommandes.toArray()[index].nativeElement.textContent = "Afficher le contenu de la commande"

    }
    this._orderService.getAnOrderItems(orderId).subscribe(items => {
      this._anOrderItems = items;
      //console.log(JSON.stringify(this._anOrderItems))
    })
  }


  get contenuCommandes(): QueryList<ElementRef> {
    return this._contenuCommandes;
  }

  set contenuCommandes(value: QueryList<ElementRef>) {
    this._contenuCommandes = value;
  }

  get boutonContenuCommandes(): QueryList<ElementRef> {
    return this._boutonContenuCommandes;
  }

  set boutonContenuCommandes(value: QueryList<ElementRef>) {
    this._boutonContenuCommandes = value;
  }

  get orderService(): OrderService {
    return this._orderService;
  }

  set orderService(value: OrderService) {
    this._orderService = value;
  }

  get anOrderItems(): OrderItem[] {
    return this._anOrderItems;
  }

  set anOrderItems(value: OrderItem[]) {
    this._anOrderItems = value;
  }

  get listeCommandes(): Order[] {
    return this._listeCommandes;
  }

  set listeCommandes(value: Order[]) {
    this._listeCommandes = value;
  }


  protected readonly JSON = JSON;

}
