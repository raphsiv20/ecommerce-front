import {Component, Input} from '@angular/core';

@Component({
  selector: 'app-informations',
  templateUrl: './informations.component.html',
  styleUrls: ['./informations.component.scss']
})
export class InformationsComponent {

  @Input() _informationsUserConnected: any ="";

  constructor() {}

  get informationsUserConnected(): any {
    return this._informationsUserConnected;
  }

  set informationsUserConnected(value: any) {
    this._informationsUserConnected = value;
  }

  protected readonly JSON = JSON;
}
