import {Component, OnInit} from '@angular/core';
import {AuthService, User} from "@auth0/auth0-angular";
import {UserConnectedIdService} from "../../services/user-connected-id.service";
import {OrderService} from "../../services/order/order.service";
import {CartService} from "../../services/cart/cart.service.service";

@Component({
  selector: 'app-header-shared',
  templateUrl: './header-shared.component.html',
  styleUrls: ['./header-shared.component.scss']
})
export class HeaderSharedComponent  implements OnInit {

  private _isConnected: boolean = false;
  private _user!: User | null |undefined;
  private _cartQuantity!: number;


  get cartQuantity(): number {
    return this._cartQuantity;
  }

  set cartQuantity(value: number) {
    this._cartQuantity = value;
  }

  get authService(): AuthService {
    return this._authService;
  }

  set authService(value: AuthService) {
    this._authService = value;
  }

  get isConnected(): boolean {
    return this._isConnected;
  }

  set isConnected(value: boolean) {
    this._isConnected = value;
  }

  constructor(private _authService: AuthService, private _userConnectedService: UserConnectedIdService, private _cartService: CartService) {
     this._authService.isAuthenticated$.subscribe(auth => {
       this._isConnected = auth;
     });

     this._authService.user$.subscribe(user => {
       //console.log(user)
     })
  }

  logout() {
    this._authService.logout();
  }

  ngOnInit(): void {
    this._userConnectedService.userConnectedCartId.subscribe(cartId => {
      if (cartId !== 0) {
        this._cartService.getACartById(cartId).subscribe(cart => {
          this._cartQuantity = cart.totalQuantity
        })
      }
    })
  }


  alertNouveautes() {
    alert("Cette fonctionnalité n'est pas encore disponible!")
  }
}
