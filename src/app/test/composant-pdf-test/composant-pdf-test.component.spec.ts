import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ComposantPdfTestComponent } from './composant-pdf-test.component';

describe('ComposantPdfTestComponent', () => {
  let component: ComposantPdfTestComponent;
  let fixture: ComponentFixture<ComposantPdfTestComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [ComposantPdfTestComponent]
    });
    fixture = TestBed.createComponent(ComposantPdfTestComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
