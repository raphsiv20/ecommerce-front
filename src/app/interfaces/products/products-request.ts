export interface ProductsRequest {
  productName?: string
  productDescription?: string
  productPrice?: number
  productImage?: string
  productCategory?: string
  productStock?: number;
}
