export interface ProductsResponse {
  _productId: number
  _productName: string
  _productDescription: string
  _productPrice: number
  _productImage: string
  _productCategory: string
  _stock: number
}
