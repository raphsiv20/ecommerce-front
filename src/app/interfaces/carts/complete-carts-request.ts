
export interface CompleteCartsRequest {
  user: any
  totalPrice: number
  totalQuantity: number
}
