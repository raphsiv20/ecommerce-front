export interface UserCartsRequest {
  user: { [key: string]: number };
}
