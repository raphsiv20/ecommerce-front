
export interface CartsResponse {
  cartId: number
  user: any
  totalPrice: number
  totalQuantity: number
}
