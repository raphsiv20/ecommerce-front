
export interface CompleteOrderRequest {
  user: any
  orderPrice: number
  orderQuantity: number
  orderDate: string
  orderShippingStatus: string
  orderPaymentStatus: string

}
