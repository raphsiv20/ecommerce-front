export interface UserDatabaseResponse {
  userId: number
  userEmail: string
}
